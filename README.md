# Hyperparameter Tuning

This project contains the code for comparing the efficiency of optimization tools, based on genetic algorithms, with other optimization methods for the hyperparameter tuning problem.

## Features

- Examples of hyperparameter tuning using genetic algorithms (TPOT), halving grid search and random search (scikit-learn).
- Optimization of the different types of neural networks (convolutional neural network, multilayer perceptron classifier and regressor).
- Data retrieving and preprocessing.

## Development Environment

| Frameworks/Libraries | Version | Branch | Commit | License | Resource |
| ------ | ------ | ------ | ------ | ------ | ------ |
| [scikit-learn](https://scikit-learn.org/stable/index.html) | 1.2.2 | [main](https://github.com/scikit-learn/scikit-learn) | [9aaed49](https://github.com/scikit-learn/scikit-learn/commit/9aaed498795f68e5956ea762fef9c440ca9eb239) | BSD-3-Clause | [scikit-learn](https://github.com/scikit-learn/scikit-learn) |
| [TensorFlow](https://www.tensorflow.org/) | 2.12.0 | [master](https://github.com/tensorflow/tensorflow) | [0db597d](https://github.com/tensorflow/tensorflow/commit/0db597d0d758aba578783b5bf46c889700a45085) | Apache-2.0 | [tensorflow](https://github.com/tensorflow/tensorflow) |
| [TPOT](https://github.com/EpistasisLab/tpot) | 0.12.0 | [add-keras-cnn-classifier](https://github.com/yonehai/tpot/tree/add-keras-cnn-classifier) | [2a33a1f](https://github.com/yonehai/tpot/commit/2a33a1fc28bd40c7fd77b3c5bf1fc4f29619c618) | LGPL-3.0 | [tpot](https://github.com/yonehai/tpot) |

## Running Instruction for Windows 10

1. Install Python 3.11 from the [official website](https://www.python.org/downloads/windows/).
2. Install pip using this [instruction](https://phoenixnap.com/kb/install-pip-windows).
3. Install [Jupyter Notebook](https://jupyter.org/install#jupyter-notebook).
4. Open the terminal, type `jupyter notebook`, and click `Enter`.
5. Open the notebook and select `Cell > Run All`.
